#include "teZZt.h"
#include "heap.h"
#include <string.h>
#include <limits.h>
#include "sorting.h"

/*
   - decommenter au fur et a mesure que vous suivez l enonce et que le code est ecrit
   - compiler avec gcc *.c -g -Wall -Wextra -o executable
*/


BEGIN_TEST_GROUP(add_to_heap_testing)

TEST(add_to_null_heap) {
      printf("\n\n\033[7;33m[add_to_null_heap]\033[0m\n"); 
      error_code_t error_code = none;
      heap_t myheap = heap_create(-1, &error_code);

      REQUIRE(myheap.tab == NULL);
      REQUIRE(error_code == allocation_error);
      
      build_min_heap(myheap, &error_code);
      REQUIRE(myheap.tab == NULL);
      REQUIRE(error_code == null_ptr_error);

      heap_data_t to_add = {5};
      heap_add(&myheap, to_add, &error_code);

      REQUIRE(myheap.tab == NULL);
      REQUIRE(error_code == null_ptr_error);
   }

TEST(adding_to_empty_heap_and_double_release) {
   printf("\n\n\033[7;33m[adding_to_empty_heap_and_double_release]\033[0m\n"); 
   error_code_t er_code = none;
   heap_t myheap = heap_create(12, &er_code);

   REQUIRE(myheap.tab != NULL);
   REQUIRE(myheap.capacity == 12);
   REQUIRE(myheap.last_position == -1);
   REQUIRE(er_code == none);


   //add on an empty heap
   heap_data_t d = {10};
   heap_add(&myheap, d, &er_code);
   CHECK(myheap.last_position == 0);
   REQUIRE(myheap.tab[myheap.last_position].element == 10);
   release_heap(&myheap);
   release_heap(&myheap);
}

TEST(adding_multiple_elements) {
   printf("\n\n\033[7;33m[adding_multiple_elements]\033[0m\n"); 
   error_code_t er_code = none;
   heap_t myheap = heap_create(12, &er_code);
   int array[11] = {17, 15, 13, 9, 6, 5, 10, 4, 8, 3, 1};

   heap_data_t data[11];
   for(int i = 0; i < 11; ++i) {
      data[i].element = array[i];
   }

   for(int i = 0; i < 11; ++i) {
      heap_add(&myheap, data[i], &er_code);
   }

   //number of elements stocked in the heap
   CHECK(myheap.last_position == 10);
   //plus petit element en racine
   CHECK(myheap.tab[0].element == 1);


   char buffer[1024];
   FILE * file = fmemopen(buffer, 1024, "w");
   REQUIRE ( NULL != file);
   heap_print(myheap, file);
   fclose(file);

   CHECK( 0 == strcmp(buffer, "1 3 6 8 4 15 10 17 9 13 5 "));
   release_heap(&myheap);
}

TEST(realloc_tests) {
   printf("\n\n\033[7;33m[realloc_tests]\033[0m\n"); 
   error_code_t er_code = none;
   heap_t myheap = heap_create(5, &er_code);
   
   REQUIRE(myheap.tab != NULL);
   REQUIRE(myheap.capacity == 5);
   REQUIRE(myheap.last_position == -1);
   REQUIRE(er_code == none);

   int array[11] = {17, 15, 13, 9, 6, 5, 10, 4, 8, 3, 1};
   heap_data_t data[11];
   for(int i = 0; i < 11; ++i) {
      data[i].element = array[i];
   }
   
   for(int i = 0; i < 5; ++i) {
      heap_add(&myheap, data[i], &er_code);
   }

   CHECK(myheap.last_position == 4);
   CHECK(is_heap_full(&myheap));

   for(int i = 5; i < 11; ++i) {
      heap_add(&myheap, data[i], &er_code);
   }

   //number of elements stocked in the heap
   CHECK(myheap.last_position == 10);
   //plus petit element en racine
   CHECK(myheap.tab[0].element == 1);

   char buffer[1024];
   FILE * file = fmemopen(buffer, 1024, "w");
   REQUIRE ( NULL != file);
   heap_print(myheap, file);
   fclose(file);

   CHECK( 0 == strcmp(buffer, "1 3 6 8 4 15 10 17 9 13 5 "));
   release_heap(&myheap);

}

END_TEST_GROUP(add_to_heap_testing)

BEGIN_TEST_GROUP(heap_build)
   TEST(build_heap) {
      printf("\n\n\033[7;33m[build_heap]\033[0m\n"); 
      error_code_t error_code = none;
      heap_t myheap = heap_create(20, &error_code);

      REQUIRE(myheap.tab != NULL);
      REQUIRE(myheap.capacity == 20);

      int array[7] = {7, 6, 5, 4, 1, 2, 3};
      for(int i = 0; i < 7; ++i) {
         heap_data_t data;
         data.element = array[i];
         myheap.tab[i] = data;
         myheap.last_position++;
      }

      build_min_heap(myheap, &error_code);
      
      REQUIRE(myheap.tab[0].element == 1);

      char buffer[1024];
      FILE * file = fmemopen(buffer, 1024, "w");
      REQUIRE ( NULL != file);
      heap_print(myheap, file);
      fclose(file);

      release_heap(&myheap);
   }

   TEST(build_heap_empty_array) {
      printf("\n\n\033[7;33m[build heap from null array]\033[0m\n"); 
      error_code_t error_code = none;
      heap_t myheap = heap_create(-1, &error_code);

      REQUIRE(myheap.tab == NULL);
      REQUIRE(error_code == allocation_error);
      
      build_min_heap(myheap, &error_code);
      REQUIRE(myheap.tab == NULL);
      REQUIRE(error_code == null_ptr_error);
      release_heap(&myheap);
   }  

   TEST(heap_extract_minimum_or_default_from_null_heap) {
      printf("\n\n\033[7;33m[build heap from null array]\033[0m\n"); 
      error_code_t error_code = none;
      heap_data_t def_value = {INT_MIN};

      heap_t myheap = heap_create(-1, &error_code);
      REQUIRE((is_heap_empty(&myheap)));

      heap_data_t min = heap_extract_min_or_default(&myheap, def_value, &error_code);
      REQUIRE(error_code == null_ptr_error);
      CHECK(min.element == def_value.element);
      release_heap(&myheap);
   }

   TEST(heap_extract_minimum_or_default_from_empty_heap) {

      printf("\n\n\033[7;33m[build heap from null array]\033[0m\n"); 
      error_code_t error_code = none;
      heap_data_t def_value = {INT_MIN};

      heap_t myheap = heap_create(20, &error_code);
      REQUIRE((is_heap_empty(&myheap)));

      heap_data_t min = heap_extract_min_or_default(&myheap, def_value, &error_code);
      REQUIRE(error_code == empty_heap);
      CHECK(min.element == def_value.element);
      release_heap(&myheap);
   }

   TEST(heap_extract_minimum_or_default) {
      printf("\n\n\033[7;33m[heap_extract_minimum_or_default]\033[0m\n"); 
      error_code_t error_code = none;
      heap_data_t defaultvalue = {INT_MAX};
      heap_t myheap = heap_create(20, &error_code);

      REQUIRE(myheap.tab != NULL);
      REQUIRE(myheap.capacity == 20);

      int array[7] = {7, 6, 5, 4, 1, 2, 3};
      int tmp  [7] = {1, 2, 3, 4, 5, 6, 7};
      for(int i = 0; i < 7; ++i) {
         heap_data_t data;
         data.element = array[i];
         myheap.tab[i] = data;
         myheap.last_position++;
      }

      build_min_heap(myheap, &error_code);
      REQUIRE(error_code == none);
      int i = 0;
      while(!(is_heap_empty(&myheap))) {
         heap_data_t min = heap_extract_min_or_default(&myheap, defaultvalue, &error_code);
         printf("%d == %d\n", min.element, tmp[i]);
         CHECK(min.element == tmp[i++]);
      }
      release_heap(&myheap);
   }

   TEST(heap_return_minimum_or_default_without_deleting) {
      printf("\n\n\033[7;33m[heap_return_minimum_or_default_without_deleting]\033[0m\n"); 
      error_code_t error_code = none;
      heap_data_t defaultvalue = {INT_MAX};
      heap_t myheap = heap_create(20, &error_code);

      int array[7] = {7, 6, 5, 4, 1, 2, 3};
      int tmp  [7] = {1, 2, 3, 4, 5, 6, 7};
      for(int i = 0; i < 7; ++i) {
         heap_data_t data;
         data.element = array[i];
         myheap.tab[i] = data;
         myheap.last_position++;
      }
      build_min_heap(myheap, &error_code);
      int i = 0;

      while(!(is_heap_empty(&myheap))) {
         heap_data_t min = heap_minimum_or_default(myheap, defaultvalue, &error_code);
         heap_extract_min_or_default(&myheap, defaultvalue, &error_code);
         printf("%d == %d\n", min.element, tmp[i]);
         CHECK(min.element == tmp[i++]);
      }
      release_heap(&myheap);
   }

   TEST(return_minimum_or_default_without_deleting_from_null_heap) {
      printf("\n\n\033[7;33m[heap_return_minimum_or_default_without_deleting]\033[0m\n"); 
      error_code_t error_code = none;
      heap_data_t defaultvalue = {INT_MAX};
      heap_t myheap = heap_create(-1, &error_code);

      build_min_heap(myheap, &error_code);

      REQUIRE(error_code == null_ptr_error);
      
      //reinitialisation du code d'erreur
      error_code = none;
      heap_data_t min = heap_minimum_or_default(myheap, defaultvalue, &error_code);
      heap_extract_min_or_default(&myheap, defaultvalue, &error_code);

      REQUIRE(error_code == null_ptr_error);
      CHECK(min.element == defaultvalue.element);
   
      release_heap(&myheap);
   }

   TEST(return_minimum_or_default_without_deleting_from_empty_heap) {
      printf("\n\n\033[7;33m[return_minimum_or_default_without_deleting_from_empty_heap]\033[0m\n"); 
      error_code_t error_code = none;
      heap_data_t defaultvalue = {INT_MAX};
      heap_t myheap = heap_create(20, &error_code);

      build_min_heap(myheap, &error_code);

      REQUIRE(error_code == empty_heap);
      
      //reinitialisation du code d'erreur
      error_code = none;
      heap_data_t min = heap_minimum_or_default(myheap, defaultvalue, &error_code);
      heap_extract_min_or_default(&myheap, defaultvalue, &error_code);

      REQUIRE(error_code == empty_heap);
      CHECK(min.element == defaultvalue.element);
   
      release_heap(&myheap);
   }


   TEST(adding_multiple_elements_and_extract) {
   printf("\n\n\033[7;33m[adding_multiple_elements]\033[0m\n"); 
   error_code_t er_code = none;
   heap_data_t defaultvalue = {INT_MAX};

   heap_t myheap = heap_create(12, &er_code);
   int array[11] =  {17, 15, 13, 9, 6, 5, 10, 4, 8, 3, 1};
   int sorted[11] = {1, 3, 4, 5, 6, 8, 9, 10, 13, 15, 17};
   heap_data_t data[11];

   for(int i = 0; i < 11; ++i) {
      data[i].element = array[i];
   }

   for(int i = 0; i < 11; ++i) {
      heap_add(&myheap, data[i], &er_code);
   }

   char buffer[1024];
   FILE * file = fmemopen(buffer, 1024, "w");
   REQUIRE ( NULL != file);
   heap_print(myheap, file);
   fclose(file);

   CHECK( 0 == strcmp(buffer, "1 3 6 8 4 15 10 17 9 13 5 "));

   int i = 0;
   while(!(is_heap_empty(&myheap))) {
      heap_data_t min = heap_extract_min_or_default(&myheap, defaultvalue, &er_code);
      REQUIRE(min.element == sorted[i++]);
   }


   release_heap(&myheap);
}
END_TEST_GROUP(heap_build)

int main(void) {
	RUN_TEST_GROUP(add_to_heap_testing); 
   RUN_TEST_GROUP(heap_build);
 	return 0;
}
