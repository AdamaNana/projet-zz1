#include "graphe.h"
#include "partition.h"
#include "matrix.h"


edge_t create_edge(int x, int y, int w) {
    edge_t a;
    a.i = x;
    a.j = y;
    a.weight = w;
    return a;
}

void init_graphe(graphe_t *g, int n, int nb_edges_max, error_code_t *err) {
    g->edges_list = malloc(nb_edges_max * sizeof(edge_t));
    g->nb_nodes = n;
    g->nb_edges = 0;
    if(g->edges_list == NULL) {
        *err = allocation_error;
        fprintf(stderr, "ERREUR : L'initialisation du graphe a échoué\n");
    } else {
        for(int i = 0; i < nb_edges_max; ++i) {
            g->edges_list[i] = create_edge(0, 0, 0);
        }
    }
}

void release_graph(graphe_t * graph) {
    free(graph->edges_list);
    graph->edges_list = NULL;
}

void draw_graph_from_adj_matrix(int **mat, int capcaity, const char * filename) {
    FILE *f_in;
    if((f_in = fopen(filename, "w")) == NULL) {
        fprintf(stderr, "ERREUR : Impossible d'ouvrir le fichier %s en écriture\n", filename);
    } else {
        fprintf(f_in, "graph G {\n");
        for(int i = 0; i < capcaity; i++) {
            for(int j = 0; j < capcaity; j++) {
                if(mat[i][j] == 1) {
                    fprintf(f_in, "%d -- %d\n", i, j);
                }
            }
        }
        fprintf(f_in, "}\n");
    }
    fclose(f_in);
}

void draw_graph(graphe_t g, const char * filename) {
    FILE *f_in;
    if((f_in = fopen(filename, "w")) == NULL) {
        fprintf(stderr, "ERREUR : Impossible d'ouvrir le fichier %s en écriture\n", filename);
    } else {
        fprintf(f_in, "graph G {\n");

        for(int i = 0; i < g.nb_nodes; i++) {
            fprintf(f_in, "%d\n", i);
        }

        for(int i = 0; i < g.nb_edges; i++) {
            fprintf(f_in, "%d -- %d[label=\"%d\",weight=\"%d\"];\n", g.edges_list[i].i, g.edges_list[i].j, g.edges_list[i].weight, g.edges_list[i].weight);
        }
        fprintf(f_in, "}\n");
    }
    fclose(f_in);
}


graphe_t generate_random_graph(int capacity, int nb_edges_max, error_code_t *err) {
    int **mat = random_adjacency_matrix(capacity, err);
    graphe_t mon_graphe;
    if(!(*err)) {
        init_graphe(&mon_graphe, capacity, nb_edges_max, err);
        if(!(*err)) {
            int indice=0;
            for(int i = 0; i < capacity; i++) {
                for(int j = 0; j < capacity; j++) {
                    if(mat[i][j] == 1) {
                        mon_graphe.edges_list[indice] = create_edge(i, j, 1); //même poids pour toutes les arêtes
                        mon_graphe.nb_edges += 1;
                        indice +=1;
                    }
                }
            }
        }
    }
    release_matrix(&mat, capacity);
    return mon_graphe;
}