#include <SDL2/SDL.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


void drawInclined() {
    
    
    SDL_Window ** windows  = NULL;
    SDL_Window ** windows2 = NULL;
    
    int width = 150;
    int height = 150;

    SDL_DisplayMode current_screen;

    int display_num = 0;

    int should_be_zero = SDL_GetCurrentDisplayMode(display_num, &current_screen);
    
    if(should_be_zero) {
        SDL_Log("Could not get display mode for video display #%d: %s", display_num, SDL_GetError());
    }

    double screen_w = current_screen.w;
    double screen_h = current_screen.h;

    int x  = 0;
    int x1 = screen_w - width; 
    int y  = 0;

    double a = screen_h / screen_w;

    int number_of_windows = 100;

    windows  = malloc(number_of_windows*sizeof(SDL_Window *));
    windows2 = malloc(number_of_windows*sizeof(SDL_Window *));


    for(int i = 0; i < number_of_windows; ++i) {
        windows[i] = SDL_CreateWindow("i-th", x, y, width, height, SDL_WINDOW_RESIZABLE);
        windows2[i] = SDL_CreateWindow("i-th", x1, y, width, height, SDL_WINDOW_RESIZABLE);
        x  = x + 15;
        x1 = x1 - 15;
        y = a*x;
    }

    SDL_Delay(10000);
    
    for(int i = number_of_windows; i > -1; --i) {
        SDL_DestroyWindow(windows[i]);
        SDL_DestroyWindow(windows2[i]);
    }



}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    SDL_DisplayMode current;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }

    drawInclined();
    SDL_Quit();

    return 0;
}