#ifndef TEST_H
#define TEST_H

#include "heap.h"
#include "p_queue.h"
#include <limits.h>
#include <time.h>


void heap_test();
void p_queue_test();


#endif