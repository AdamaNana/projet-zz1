#include "p_queue.h"



p_queue_t init_queue(int capacity, error_code_t * error_code) {
    p_queue_t queue;
    queue.heap = heap_create(capacity, error_code);
    return queue;
}

heap_data_t p_queue_top(p_queue_t *q, heap_data_t default_value, error_code_t * error_code) {
    return heap_minimum_or_default(q->heap, default_value, error_code);
}

void p_queue_pop(p_queue_t * q, heap_data_t default_value, error_code_t * error_code) {
    heap_extract_min_or_default(&(q->heap), default_value, error_code);
}

void p_queue_push(p_queue_t * q, heap_data_t data, error_code_t * er_code){
    heap_add(&(q->heap), data, er_code);
}

void p_queue_release(p_queue_t * q) {
    release_heap(&(q->heap));
    q = NULL;
}

int p_queue_is_empty(p_queue_t * q) {
    return is_heap_empty(&(q->heap));
}


