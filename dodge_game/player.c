#include "player.h"



player_t create_player(vect2D_t pos, SDL_Rect destination) {
    player_t player1;

    player1.destination = destination;
    player1.position = pos;

    return player1;
}



player_t update_player_coordinates(player_t player, p_orientation_n p_direction, SDL_Rect window_dimension) {
    switch(p_direction) {
        case RIGHT:
            player.destination.x = player.destination.x + 10;
            if(player.destination.x > window_dimension.w) {
                player.destination.x = 0;
            }
            break;
        case LEFT:
            player.destination.x = player.destination.x - 10;
            if(player.destination.x < 0) {
                player.destination.x = window_dimension.w;
            }
            break;
        case NEUTRAL:
            player.destination.x = player.destination.x;
            break;
    }
    return player;
}