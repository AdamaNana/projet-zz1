#ifndef HEAP_H
#define HEAP_H

#include <stdio.h>
#include <stdlib.h>
#include "utile.h"


typedef struct heap heap_t;
typedef struct heap_data heap_data_t;

struct heap_data {
    int element;
};

struct heap {
    int capacity;
    int last_position;
    heap_data_t * tab;
};


//Building and init primitives
heap_t  heap_create(int capacity, error_code_t * error_code);
void    build_min_heap(heap_t heap, error_code_t * error_code);
void    heap_add(heap_t * heap, heap_data_t data, error_code_t * error_code);
void    min_heapify_up(heap_t heap, int node);
void    min_heapify_down(heap_t heap, int node);


//extracting from the heap
heap_data_t heap_minimum_or_default(heap_t heap, heap_data_t default_value, error_code_t * error_code);
heap_data_t heap_extract_min_or_default(heap_t * heap, heap_data_t default_value, error_code_t * error_code);


//other usefull operations
void    swap(heap_t heap, int from, int to);
int     left_child(int node); //2*node + 2
int     right_child(int node); //2*node + 1
int     get_parent(int node); //node - 1 / 2
void    heap_print(heap_t heap, FILE * stream);
int     is_heap_empty(heap_t * heap);
void    heap_sort(heap_t * heap, error_code_t * error_code);
int     is_heap_full(heap_t * heap);
void    heap_expand_size(heap_t * heap, error_code_t * error_code);


//memory release
void    release_heap(heap_t * heap);


/* to review after if needed */
//int compare(heap_data_t element1, heap_data_t element2);


#endif