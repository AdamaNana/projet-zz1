#include "vect.h"


vect2D_t init_vect(double x, double y) {
    vect2D_t this;
    this.x = x;
    this.y = y;
    return this;
}


vect2D_t add(vect2D_t v1, vect2D_t v2) {
    vect2D_t res;
    res.x = v1.x + v2.x;
    res.y = v1.y + v2.y;
    return res;
}
