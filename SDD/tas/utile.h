#ifndef UTILE_H
#define UTILE_H


typedef enum error_code error_code_t;

enum error_code {
    none,
    allocation_error,
    empty_heap,
    null_ptr_error
};

#endif