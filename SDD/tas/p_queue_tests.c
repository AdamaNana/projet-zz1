#include "teZZt.h"
#include "p_queue.h"
#include <string.h>
#include <limits.h>
#include <time.h>

BEGIN_TEST_GROUP(p_queue_testing)

    TEST(queue_initialisation_normal) {
        printf("\n\n\033[7;33m[normal_queue_initialisation]\033[0m\n"); 
        error_code_t error_code = none;
        int capacity = 10;
        p_queue_t q = init_queue(capacity, &error_code);
        REQUIRE(error_code == none);
        REQUIRE(q.heap.capacity == capacity);

        p_queue_release(&q);
    }

    TEST(queue_initialisation_zero_capacity) {
        printf("\n\n\033[7;33m[queue_initialisation_zero_capacity]\033[0m\n"); 
        error_code_t error_code = none;
        int capacity = 0;
        p_queue_t q = init_queue(capacity, &error_code);
        REQUIRE(error_code == none);
        REQUIRE(q.heap.capacity == capacity);

        p_queue_release(&q);
    }

    TEST(queue_initialisation_unvalid_capacity) {
        printf("\n\n\033[7;33m[queue_initialisation_unvalid_capacity]\033[0m\n"); 
        error_code_t error_code = none;
        int capacity = -1;
        p_queue_t q = init_queue(capacity, &error_code);
        REQUIRE(error_code == allocation_error);
        REQUIRE(q.heap.capacity == capacity);

        p_queue_release(&q);
    }

    TEST(p_queue_push_and_popping) {

        printf("\n\n\033[7;33m[p_queue_push_and_popping]\033[0m\n"); 
        error_code_t error_code = none;
        int capacity = 10;
        p_queue_t q = init_queue(capacity, &error_code);
        heap_data_t default_value = {INT_MAX};

        REQUIRE(error_code == none);
        REQUIRE(q.heap.capacity == capacity);

        int array[14] =  {17, 15, 13, 9, 6, 5, 10, 4, 8, 3, 1, 16, 11, 12};
        int sorted[14] = {1, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 15, 16, 17};

        for(int i = 0; i < 14; ++i) {
            heap_data_t data = {array[i]};
            p_queue_push(&q, data, &error_code);
        }

        int i = 0;
        while(!p_queue_is_empty(&q)) {
            heap_data_t data = p_queue_top(&q, default_value, &error_code);
            p_queue_pop(&q, default_value, &error_code);
            REQUIRE(data.element == sorted[i++]);
        }

        p_queue_release(&q);
    }


    TEST(p_queue_push_and_popping_empty_queue) {

        printf("\n\n\033[7;33m[p_queue_push_and_popping_empty_queue]\033[0m\n"); 
        error_code_t error_code = none;
        int capacity = 10;
        p_queue_t q = init_queue(capacity, &error_code);
        heap_data_t default_value = {INT_MAX};

        REQUIRE(error_code == none);
        REQUIRE(q.heap.capacity == capacity);
        REQUIRE(p_queue_is_empty(&q));
        
        //pop test
        p_queue_pop(&q, default_value, &error_code);
        REQUIRE(error_code == empty_heap);

        //push test
        error_code = none;
        p_queue_top(&q, default_value, &error_code);
        REQUIRE(error_code == empty_heap);

        p_queue_release(&q);
    }


    TEST(p_queue_push_and_popping_null_queue) {

        printf("\n\n\033[7;33m[p_queue_push_and_popping_null_queue]\033[0m\n"); 
        error_code_t error_code = none;
        int capacity = -1;
        p_queue_t q = init_queue(capacity, &error_code);
        heap_data_t default_value = {INT_MAX};

        REQUIRE(error_code == allocation_error);
        REQUIRE(q.heap.capacity == capacity);
        REQUIRE(p_queue_is_empty(&q));
        
        //pop test
        p_queue_pop(&q, default_value, &error_code);
        REQUIRE(error_code == null_ptr_error);

        //push test
        error_code = none;
        p_queue_top(&q, default_value, &error_code);
        REQUIRE(error_code == null_ptr_error);

        p_queue_release(&q);
    }

END_TEST_GROUP(p_queue_testing)


int main() {
    RUN_TEST_GROUP(p_queue_testing);
    return 0;
}