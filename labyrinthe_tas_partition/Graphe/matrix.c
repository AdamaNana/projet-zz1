#include "matrix.h"


int ** init_matrix(int capacity, error_code_t *err) {

    int **mat = malloc(capacity * sizeof(int *));
    int i = 0;
    if(mat != NULL) {
        for(i = 0; i < capacity; ++i) {
            mat[i] = malloc(capacity * sizeof(int));
            if(mat[i] == NULL) {
                break;
                *err = allocation_error;
            }
        }
        if(*err == allocation_error) {
            for(int tmp = i; tmp > -1; --tmp) {
                free(mat[tmp]);
            }
            mat = NULL;
        }
    }
    else {
        *err = allocation_error;
    }
    return mat;
}


int ** random_adjacency_matrix(int capacity, error_code_t *err) {
    srand(time(NULL));
    int **mat = init_matrix(capacity, err);
    if(*err != allocation_error) {
        for(int i = 0; i < capacity; i++) {
            for(int j = 0; j < capacity; j++) {
                if(i>j)
                    mat[i][j] = rand()%2;
                else
                    mat[i][j] = 0;
            }
        }
    }
    return mat;
}


void print_matrice(int **mat, int capacity) {
    for(int i=0; i<capacity; i++) {
        for(int j=0; j<capacity; j++) {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}

void release_matrix(int *** mat, int capacity) {
    for(int i = 0; i < capacity; ++i) {
        free((*mat)[i]);
    } 
    free(*mat);
    *mat = NULL;
}