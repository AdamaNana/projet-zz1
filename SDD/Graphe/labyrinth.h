#ifndef LABYRINTH_H
#define LABYRINTH_H

#include "graph_algos.h"
#include "labyrinth.h"

#define SUD   2
#define NORD  1
#define EST   4
#define OUEST 8

typedef struct maze {
    int ** mat;
    graphe_t graph;
}maze_t;

maze_t generate_random_maze(int n, int p, error_code_t * err, predicat_organizing_edges func_edge_sort_perm);
maze_t allocate_maze(int n, int p, error_code_t * err);
void init_maze(maze_t * maze, int n, int p, graphe_t starting_graph, predicat_organizing_edges func, error_code_t * err);
void print_maze(maze_t * maze, int n, int p, error_code_t * err);
void release_maze(maze_t * maze, int p);
int est_valide(int i, int taille);
int does_graphe_contain_edge(graphe_t g, int x, int y);
graphe_t create_graphe_from_matrix(int n, int p);

int get_row_index(int node, int L);
int get_column_index(int node, int L);

#endif