#include "labyrinth.h"


maze_t allocate_maze(int n, int p, error_code_t * err) {
    
    maze_t m = {NULL};
    m.mat   = malloc(n * sizeof(int *));
    m.graph.edges_list = NULL;
    m.graph.nb_edges = 0;
    m.graph.nb_nodes = 0;

    if(m.mat != NULL) {
        for(int i = 0; i < n; ++i) {
            m.mat[i] = malloc(p*sizeof(int));
            if(m.mat[i] == NULL) {
                *err = allocation_error;
                for(int tmp = i; tmp > -1; --tmp) {
                    free(m.mat[tmp]);
                }
                free(m.mat);
                break;
            }
        }
    }
    else {
        *err = allocation_error;
    }

    return m;
}

void init_maze(maze_t * maze, int n, int p, graphe_t starting_graph, predicat_organizing_edges func, error_code_t * err) {
    if(*err == none) {
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < p; ++j) {
                maze->mat[i][j] = 15;
            }
        }
        maze->graph = kruskal(starting_graph, err, func);
    }
}

void print_maze(maze_t * maze, int n, int p, error_code_t * err) {
    if(*err == none) {
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < p; ++j) {
                printf("%d ", maze->mat[i][j]);
            }
            printf("\n");
        }
    }
}

graphe_t create_graphe_from_matrix(int n, int p) {
    graphe_t g = {NULL, 0, 0};
    int nb_edges_max = pow(n*p, 2) / 2;
    error_code_t err = none;
    init_graphe(&g, n*p, nb_edges_max, &err);
    int droite, bas, k=0;

    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < p; ++j) {

            int current_node = i*p + j;
            if (j + 1 < p) {
                droite = (i * p) + j + 1;
                g.edges_list[k++] = create_edge(current_node, droite, 1);
            }
            if(i + 1 < n) {
                bas = (i + 1) * p + j;
                g.edges_list[k++] = create_edge(current_node, bas, 1);
            }
        }
    }

    //update nodes
    g.nb_edges = k;
    draw_graph(g, "graph_imgs/A.gv");
    return g;
}

maze_t generate_random_maze(int n, int p, error_code_t * err, predicat_organizing_edges func_edge_sort_perm) {

    //generation d'un graphe et calcul de l'arbre couvrant
    graphe_t g   = create_graphe_from_matrix(n,p);
    
    //creation du labyrinthe
    maze_t mymaze = allocate_maze(n, p, err);
    init_maze(&mymaze, n, p, g, func_edge_sort_perm, err);
    
    draw_graph(mymaze.graph, "graph_imgs/DA_MAAZE.gv");
    
    for(int i = 0; i < mymaze.graph.nb_edges; ++i) {
        
        int node1 = mymaze.graph.edges_list[i].i;
        int node2 = mymaze.graph.edges_list[i].j;
        if ( node1 < node2 ) //node1 avant le node2 ?
        {   
            if ( get_row_index(node1,p) < get_row_index(node2,p)) // est-ce que node2 en bas du node1 car node1.ligne < node2.ligne
            {
                mymaze.mat[get_row_index(node1,p)][get_column_index(node1,p)] -= SUD;
                mymaze.mat[get_row_index(node2,p)][get_column_index(node2,p)] -= NORD;
            }     
            else{ //ils auront meme hauters dont test gauch ou droite
                mymaze.mat[get_row_index(node1,p)][get_column_index(node1,p)] -= EST;
                mymaze.mat[get_row_index(node2,p)][get_column_index(node2,p)] -= OUEST;
            }       
        }
        else{ //node2 avant node1
            if ( get_row_index(node2,p) < get_row_index(node1,p)) // est-ce que node1 en bas du node2 car node2.ligne < node1.ligne
            {
                mymaze.mat[get_row_index(node2,p)][get_column_index(node2,p)] -= SUD;
                mymaze.mat[get_row_index(node1,p)][get_column_index(node1,p)] -= NORD;
            }     
            else{
                mymaze.mat[get_row_index(node2,p)][get_column_index(node2,p)] -= EST;
                mymaze.mat[get_row_index(node1,p)][get_column_index(node1,p)] -= OUEST;
            }
        }

    }
    
    release_graph(&g);
    return mymaze;
}

void release_maze(maze_t * maze, int p) {
    release_matrix(&(maze->mat), p);
    release_graph(&(maze->graph));
}

int get_row_index(int node, int L) {
    return node / L;
}

int get_column_index(int node, int L) {
    return node % L;
}

