#ifndef ENGINE_H
#define ENGINE_H

#include "grid.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

void life_game(int ** grid, int ** copy, int rows, int cols);
void game(int ** grid, int capacity);
int count_neighbors(int ** grid, int i, int j, int rows, int cols);

void load_config(int config_num, SDL_Window * window, SDL_Renderer * renders);
void pick_grid_config(int config_num, int ** grid, int rows, int cols);
void pick_rows_cols(int config_num, int * rows, int * cols);

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer);
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer );
void draw_background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer);

#endif 