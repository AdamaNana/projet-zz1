#ifndef GRID_Hr
#define GRID_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int ** init_gride(int cols, int rows, int * error_code);
void fill_grid(int ** grid, int rows, int cols);
void show_grid(int ** grid, int rows, int cols);
void free_grid(int *** grid, int rows, int cols);
void copy_array(int ** src, int ** dest, int rows, int cols);

#endif