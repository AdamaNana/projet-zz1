#include "engine.h"


/* RULES */
//si cellule entouré par 2 / 3 voisine --> survie
//si cellule entouré par 3 voisine exactement naissance
//si cellule entouré par plus que 3 --> mort


void pick_rows_cols(int config_num, int * rows, int * cols) {

    switch(config_num) {
        case 1: //Glider
            *rows = 20;
            *cols = 20;
            break;
        case 2: //Spaceship
            *rows = 45;
            *cols = 25;
            break;
        case 3: //Blinkers
            *rows = 20;
            *cols = 30;
            break;
        default: //aleatoire
            *rows = 30;
            *cols = 30;
            break;
    }
}

void pick_grid_config(int config_num, int ** grid, int rows, int cols) {

    switch(config_num) {
        case 1: //Glider
            grid[15][5] = 1;
            grid[14][6] = 1;
            grid[13][4]  = 1;
            grid[13][5] = 1;
            grid[13][6] = 1;
            break;
        case 2: //Spaceship
            grid[36][1]  = 1; grid[37][2]  = 1; grid[37][3]  = 1;
            grid[37][4]  = 1; grid[37][5]  = 1; grid[37][6]  = 1;
            grid[36][6]  = 1; grid[35][6]  = 1; grid[34][5] = 1;
            
            grid[3][2] = 1; grid[4][3] = 1; grid[4][4]  = 1;
            grid[4][5] = 1; grid[4][6] = 1; grid[3][6] = 1;
            grid[2][6]  = 1; grid[1][5] = 1;
            break;
        case 3: //Blinkers
            grid[7][4]   = 1; grid[8][5]    = 1; grid[8][6]    = 1;
            grid[9][3]   = 1; grid[9][4]    = 1; grid[10][5]   = 1;
            grid[9][14]  = 1; grid[9][15]   = 1; grid[9][16]   = 1;
            grid[8][15]  = 1; grid[10][15]  = 1; grid[9][19]   = 1;
            grid[9][20]  = 1; grid[10][19]  = 1; grid[10][20]  = 1;
            grid[8][21]  = 1; grid[8][22]   = 1; grid[7][21]   = 1; grid[7][22]  = 1;
            break;
        default: //aleatoire
            fill_grid(grid, rows, cols);
            break;
    }
}

void load_config(int config_num, SDL_Window * window, SDL_Renderer * renders) {

    int error_code = 0;
    SDL_Rect rectangle;                                             
    SDL_bool program_on = SDL_TRUE;

    int cols   = 0;
    int rows   = 0;
    int width  = 15;
    int height = 15;

    //choix de la bonne dimension
    pick_rows_cols(config_num, &rows, &cols);

    //SDL 
    int window_w = width * cols;
    int window_h = height * rows;

    //SDL_RenderSetLogicalSize(renders, window_w, window_h);
    SDL_SetWindowSize(window, window_w, window_h);

    //grid  malloc  
    int ** grid = init_gride(rows, cols, &error_code);
    if(error_code) {
        end_sdl(0, "ERROR WINDOW CREATION", window, renders);
    }
    int ** copy = init_gride(rows, cols, &error_code);
    if(error_code) {
        end_sdl(0, "ERROR WINDOW CREATION", window, renders);
    }

    int ** svg1 = copy;
    int ** svg2 = grid;

    //initialisation de la bonne grille
    pick_grid_config(config_num, grid, rows, cols);
    int pause = 0; //mise en pause
    int mouse_x = 0, mouse_y = 0;

    while (program_on){                       
        SDL_Event event;                        
        while(program_on && SDL_PollEvent(&event)){                                                                      
            switch(event.type){  
                //touche clavier                    
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){ 
                        case SDLK_ESCAPE:
                            program_on = SDL_FALSE;
                            break;
                        case SDLK_p :
                        pause = !pause;
                            break;
                        case SDLK_SPACE :
                            pause = !pause;
                            break;
                        default:
                            break;
                }
                //souris
                case SDL_MOUSEBUTTONDOWN :
                    if(SDL_GetMouseState(&mouse_x, &mouse_y) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
                        grid[mouse_y/15][mouse_x/15] = 1;
                    }
                default:                                  
                    break;
            }
        }

        for(int i = 0; i < rows; ++i) {
            for(int j = 0; j < cols; ++j) {
                if(grid[i][j] == 1) {
                    SDL_SetRenderDrawColor(renders, 0, 0, 0, 0);
                }
                else {
                    SDL_SetRenderDrawColor(renders, 255, 255, 255, 0);
                }
                rectangle.h = height;
                rectangle.w = width;
                rectangle.x = width  * j;
                rectangle.y = height * i;
                SDL_RenderFillRect(renders, &rectangle);
            }
        }
        SDL_RenderPresent(renders);
        if(!pause) {   
            life_game(grid, copy, rows, cols);
        }
        SDL_Delay(1000/20);
        SDL_RenderClear(renders);
    }

    free_grid(&svg1, rows, cols);
    free_grid(&svg2, rows, cols);
}

void life_game(int ** grid, int ** copy, int rows, int cols) {

    int survive[8] = {0, 0, 1, 1, 0, 0, 0, 0};
    int birth[8] = {0, 0, 0, 1, 0, 0, 0, 0};
    int state = 0;

    for(int i = 0; i < rows; ++i) {
        for(int j = 0; j < cols; ++j) {
            state = grid[i][j];
            int neighbors = count_neighbors(grid, i, j, rows, cols);
            if (state == 0 && birth[neighbors] == 1) {
                copy[i][j] = 1;
            } else if (state == 1 && survive[neighbors] == 0) {
                copy[i][j] = 0;
            } else if(state == 1 && survive[neighbors] == 1){
                copy[i][j] = 1;
            } else {
                copy[i][j] = state;
            }
        }
    }
    copy_array(copy, grid, rows, cols);
}


int count_neighbors(int ** grid, int i, int j, int rows, int cols) {
    int c = 0, l = 0;
    int sum = 0;
    for(int x = -1; x < 2; ++x) {
        for(int y = -1; y < 2; ++y) {
            l = (i + x + rows) % rows;
            c = (j + y + cols) % cols;
            sum += grid[l][c];
        }
    }

    if(grid[i][j] == 1) {
        sum = sum - 1;
    }

    return sum;
}


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                


  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
} 


void draw_background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer) {

  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                   // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,
                 &source,
                 &destination);                 // Création de l'élément à afficher
}

SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}