#include "sphere.h"


boule_t create_boule(vect2D_t pos, vect2D_t vel, vect2D_t grav, SDL_Rect destination) {
    
    boule_t boule;

    boule.destination = destination;
    boule.gravity  = grav;
    boule.position = pos;
    boule.velocity = vel;
    
    return boule;
}


SDL_Rect init_destination(double w, double h, int x, int y) {
    SDL_Rect destination;
    destination.w = w;
    destination.h = h;
    destination.x = x;
    destination.y = y;

    return destination;
}

void update_bool_coordinates(boule_t * boules, int number_of_boules, SDL_Rect windows_dimensions) {
    
    for(int i = 0; i < number_of_boules; ++i) {
            boules[i].position = add(boules[i].position, boules[i].velocity);
            boules[i].velocity = add(boules[i].velocity, boules[i].gravity);

            if(boules[i].position.x > (windows_dimensions.w - (boules[i].destination.w))  || boules[i].position.x < 0) {  // droite : x < screen_w - rayon   gauche : x > rayon
                boules[i].velocity.x = boules[i].velocity.x  * (-1);
            }   
            if(boules[i].position.y + boules[i].destination.h + 75  > windows_dimensions.h ) { // window_dimensions_h - rayon
                boules[i].velocity.y = boules[i].velocity.y * (-0.95);
                boules[i].position.y = windows_dimensions.h - boules[i].destination.h - 100;
            }
        boules[i].destination.x = boules[i].position.x;
        boules[i].destination.y = boules[i].position.y;
    }
}