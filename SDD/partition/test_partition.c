#include "partition.h"
#include "teZZt.h"


BEGIN_TEST_GROUP(partition_test)

    TEST(initialisation_partition) {

        printf("\n\n\033[7;33m[initialisation_partition]\033[0m\n");
        partition_t p;
        int cap = 10;
        error_code_t err = none;

        init_partition(&p, 10, &err);

        REQUIRE(p.capacity == cap);
        REQUIRE(err == none);
        CHECK(p.parent != NULL);
        CHECK(p.height_array != NULL);

        release_partition(&p);

    }

    TEST(initialisation_partition_wrong_capacity) { 

        printf("\n\n\033[7;33m[initialisation_partition_wrong_capacity]\033[0m\n");
        partition_t p;
        int cap = -1;
        error_code_t err = none;

        init_partition(&p, cap, &err);
        REQUIRE(err == allocation_error);
        REQUIRE(p.parent == NULL);
        REQUIRE(p.height_array == NULL);

        release_partition(&p);
    }

    TEST(create_partition) {
        printf("\n\n\033[7;33m[create_partition]\033[0m\n");
        int array[5] = {1, 2, 3, 4, 5};
        int cap = 5;
        error_code_t err = none;

        partition_t p = create_partition(array, cap, &err);
        REQUIRE(err == none);

        for(int i = 0; i < cap; ++i) {
            CHECK(p.height_array[i] == 1);
            CHECK(p.parent[i] == array[i]);
        }
        release_partition(&p);
    }

    TEST(search_for_root) {
        printf("\n\n\033[7;33m[search_for_root]\033[0m\n");
        int tab[] = {0, 0, 2, 2, 5, 5, 4, 8, 5, 5, 2};
        error_code_t err = none;


        partition_t p = create_partition(tab, 11, &err);
        
        CHECK(search_for_root(&p, -1, &err) == -1);
        CHECK(err == invalid_node);

        CHECK(search_for_root(&p, 100, &err) == -1);
        CHECK(err == invalid_node);

        err = none;

        CHECK(search_for_root(&p, 7, &err) == 5);
        CHECK(search_for_root(&p, 4, &err) == 5);
        CHECK(search_for_root(&p, 0, &err) == 0);
        CHECK(search_for_root(&p, 2, &err) == 2);
        CHECK(search_for_root(&p, 10, &err) == 2);

        release_partition(&p);
    }

    TEST(fusion_empty_partition) {
        
        printf("\n\n\033[7;33m[fusion_empty_partition]\033[0m\n");
        partition_t p;
        error_code_t err = none;
        init_partition(&p, -1, &err);
        REQUIRE(err == allocation_error);

        fusion(&p, 1, 4, &err);
        CHECK(err == allocation_error);

        release_partition(&p);
    }
    
    TEST(fusion_normal_case_and_elements_in_same_class) {
        printf("\n\n\033[7;33m[fusion_normal_case_and_elements_in_same_class]\033[0m\n");
        int tab[] = {0, 0, 2, 2, 5, 5, 4, 8, 5, 5, 2};
        error_code_t err = none;  
        partition_t p = create_partition(tab, 11, &err);

        p.height_array[5] = 3;
        p.height_array[2] = 2;
        p.height_array[0] = 2;


        fusion(&p, 3, 1, &err);
        int root = search_for_root(&p,  3, &err);
        int root2 = search_for_root(&p, 1, &err);
        REQUIRE(root == root2);
        REQUIRE(p.height_array[root] == p.height_array[root2]);


        fusion(&p, 0, 1, &err);
        root  = search_for_root(&p,  0, &err);
        root2 = search_for_root(&p, 1, &err);
        REQUIRE(root == root2);
        REQUIRE(p.height_array[root] == p.height_array[root2]);

        fusion(&p, 2, 10, &err);
        root  = search_for_root(&p,  2, &err);
        root2 = search_for_root(&p, 10, &err);
        REQUIRE(root == root2);
        REQUIRE(p.height_array[root] == p.height_array[root2]);

        fusion(&p, 3, 2, &err);
        root  = search_for_root(&p,  2, &err);
        root2 = search_for_root(&p, 3, &err);
        REQUIRE(root == root2);
        REQUIRE(p.height_array[root] == p.height_array[root2]);
        
        release_partition(&p);
    }

    TEST(class_listing) {
        printf("\n\n\033[7;33m[class_listing]\033[0m\n");
        int tab[] = {0, 0, 2, 2, 5, 5, 4, 8, 5, 5, 2};
        error_code_t err = none;  
        partition_t p = create_partition(tab, 11, &err);

        p.height_array[5] = 3;
        p.height_array[2] = 2;
        p.height_array[0] = 2;

    
        char buffer[1024];
        FILE * file = fmemopen(buffer, 1024, "w");
        REQUIRE ( NULL != file);
        class_listing(&p, 5, file, &err);
        fclose(file);
        
        CHECK( 0 == strcmp(buffer, "{ 4  5  6  7  8  9 }"));
        class_listing(&p, 5, stdout, &err);
        release_partition(&p);
    }

    TEST(all_partitions_listing) {
        printf("\n\n\033[7;33m[all_partitions_listing]\033[0m\n");
        int tab[] = {0, 0, 2, 2, 5, 5, 4, 8, 5, 5, 2};
        error_code_t err = none;  
        partition_t p = create_partition(tab, 11, &err);

        p.height_array[5] = 3;
        p.height_array[2] = 2;
        p.height_array[0] = 2;

    
        char buffer[1024];
        FILE * file = fmemopen(buffer, 1024, "w");
        REQUIRE ( NULL != file);
        partitions_listing(&p, file, &err);
        fclose(file);
    
        CHECK( 0 == strcmp(buffer, "{{ 0  1 },{ 2  3  10 },{ 4  5  6  7  8  9 },}"));
        release_partition(&p);
    }

END_TEST_GROUP(partition_test)

int main() {

    RUN_TEST_GROUP(partition_test);
    return 0;
}