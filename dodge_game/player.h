#ifndef PLAYER_H
#define PLAYER_H

#include "vect.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


typedef enum orientation p_orientation_n;
enum orientation {
    LEFT,
    RIGHT,
    NEUTRAL
};

typedef struct player player_t;

struct player {
    SDL_Rect destination;
    vect2D_t position;
};

player_t create_player(vect2D_t pos, SDL_Rect destination);
player_t update_player_coordinates(player_t player, p_orientation_n direction, SDL_Rect window_dimension);



#endif