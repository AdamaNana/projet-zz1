#include "sorting.h"




void heap_sort(heap_t * heap, error_code_t * error_code) {
    build_min_heap(*heap, error_code);
    if(*error_code != null_ptr_error) {
        heap_data_t t = {4};
        int length = heap->last_position;
        while(!(is_heap_empty(heap))) {
            heap_extract_min_or_default(heap, t, error_code);
        }
        heap->last_position = length;
    }
}

void sorted_array_print(heap_t heap, FILE * stream) {

    if(heap.tab != NULL && heap.last_position != -1) {
        for(int i = 0; i <= heap.last_position; ++i) {
            fprintf(stream, "%d ", heap.tab[i].element);
        }
    }
    else {
        printf("empty heap \n");
    }
    printf("\n");
}

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)b - *(int*)a );
}

