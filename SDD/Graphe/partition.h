#ifndef PARTITION_H
#define PARTITION_H

#include "utile.h"



typedef struct partition_data partition_data_t;
typedef struct partition partition_t;
typedef struct class class_t;

struct partition {
    int * parent;
    int * height_array;
    int capacity;
};


void init_partition(partition_t * p, int capacity, error_code_t *err);
partition_t create_partition(int * array, int capacity, error_code_t *err);
int search_for_root(partition_t *p, int node, error_code_t * err);
void fusion(partition_t *p, int first_node, int second_node, error_code_t * err);
void class_listing(partition_t * p, int classe, FILE * stream, error_code_t *err);
void partitions_listing(partition_t * p, FILE * stream, error_code_t * err);
int look_for_class(partition_t *p, int node, error_code_t * err);


void release_partition(partition_t * p);


#endif