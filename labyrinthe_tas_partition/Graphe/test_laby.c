#include "graph_algos.h"
#include "labyrinth.h"


int main() {


    error_code_t err = none;
    maze_t m = generate_random_maze(20,20,&err,fisher_yate);
    print_maze(&m, 20, 20, &err);

    release_maze(&m, 20);
}