#include "grid.h"



int ** init_gride(int rows, int cols, int * error_code) {

    int ** grid = malloc(rows * sizeof(int *));
    int i = 0;
    if(grid != NULL) {
        for(i = 0; i < rows; ++i) {
            grid[i] = malloc(cols * sizeof(int));
            if(grid[i] == NULL) {
                break;
                *error_code = 1;
            }
        }
        if(*error_code) {
            for(int tmp = i; tmp > -1; --tmp) {
                free(grid[tmp]);
            }
            grid = NULL;
        }
    }
    else {
        *error_code = 1;
    }

    if(*error_code == 0) {
        for(int i = 0; i < rows; ++i) {
            for(int j = 0; j < cols; ++j)  {
                grid[i][j] = 0;
            }
        }
    }
    return grid;
}

void copy_array(int ** src, int ** dest, int rows, int cols) {
    for(int i = 0; i < rows; ++i) {
        for(int j = 0; j < cols; ++j) {
            dest[i][j] = src[i][j];
        }
    }
}

void fill_grid(int ** grid, int rows, int cols) {
    srand(time(NULL));
    int lower = 0;
    int upper = 1;
    for(int i = 0; i < rows; ++i) {
        for(int j = 0; j < cols; ++j) {
            int num = (rand() % (upper - lower + 1)) + lower;
            grid[i][j] = num;
        }
    }
}

void show_grid(int ** grid, int rows, int cols) {
    for(int i = 0; i < rows; ++i) {
        for(int j = 0; j < cols; ++j) {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
}


void free_grid(int *** grid, int rows, int cols) {

    for(int i = 0; i < rows; ++i) {
        free((*grid)[i]);
    } 
    free(*grid);
    *grid = NULL;
}