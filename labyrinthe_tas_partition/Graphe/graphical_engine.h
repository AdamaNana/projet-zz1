#ifndef GRAPHICAL_ENGINE_H
#define GRAPHICAL_ENGINE_H

#include "graph_algos.h"
#include "labyrinth.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer);
void draw_background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer);
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer );
void maze_runner_dij_a_star(SDL_Texture * bg, SDL_Window * window, SDL_Renderer * renderer, maze_t mymaze, int n , int p, int from, int to, int parcour_choice);
void show_maze(SDL_Window * window, SDL_Renderer * renderer, int n , int p, int choice);

void dfs(SDL_Window * window, SDL_Renderer * renderer, int n , int p);
void DFS_rec_graphic(SDL_Renderer * renderer, SDL_Window * window, SDL_Texture * bg, SDL_Texture * player,
                     SDL_Rect source_player, SDL_Rect destination_player, maze_t mymaze, int starting_node, 
                     colors_t * colors, int * parent, int n, int p);

#endif